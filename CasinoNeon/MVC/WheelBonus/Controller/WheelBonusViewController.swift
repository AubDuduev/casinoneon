//
//  WheelBonusViewController.swift
import SpriteKit
import UIKit

class WheelBonusViewController: UIViewController {
  
  //MARK: - Controllers
  public var controllers: WheelBonusControllers!
  
  //MARK: - Public variable
  
  public var arrayBonus   = [55, 300, 125, 0, 175, 240]//крест, алмаз, доллар, глаз, цветок, жук
  public var arraySumm    = [100, 200, 300, 400, 500, 1000]
  public var currentBonus = 0
  public var currentTrys  = 4
  public var gameScene    : SKScene!
  public var gameSceneView: SKView!
  public var callBack     : ClousureEmpty!
  
  //MARK: - Outlets
  @IBOutlet weak var wheelView   : WheelView!
  @IBOutlet weak var balanceLabel: UILabel!
  @IBOutlet weak var winLabel    : UILabel!
  @IBOutlet weak var tryButton   : UIButton!
  @IBOutlet weak var spinButton  : UIButton!
  
  //MARK: - Outlets array
  @IBOutlet var winsLabels: [UILabel]!
  @IBOutlet var winsImages: [UIImageView]!
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setControllers()
    self.controllers.setup.viewDidLoad()
    self.controllers.present.viewDidLoad()
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
  }
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    self.controllers.setup.viewDidLayoutSubviews()
  }
  private func setControllers(){
    self.controllers = WheelBonusControllers(viewController: self)
  }
}
