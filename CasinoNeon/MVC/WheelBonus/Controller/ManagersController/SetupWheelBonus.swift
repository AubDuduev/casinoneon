//
//  SetupWheelBonus.swift
import SpriteKit
import Foundation

class SetupWheelBonus: Controller {
  
  //MARK: - Public variable
  public var VC: WheelBonusViewController!
  
  public func viewDidLoad() {
    self.gameScene()
    self.gameSceneView()
  }
  public func viewDidLayoutSubviews() {
    self.wheelView()
  }
}
//MARK: - Private functions
extension SetupWheelBonus {
  
  private func gameSceneView(){
    self.VC.gameSceneView                     = SKView()
    self.VC.gameSceneView.frame               = CGRect()
    self.VC.gameSceneView.ignoresSiblingOrder = false
    self.VC.gameSceneView.showsFPS            = true
    self.VC.gameSceneView.showsNodeCount      = true
    self.VC.gameSceneView.backgroundColor     = .clear
    self.VC.view.addSubview(self.VC.gameSceneView)
    self.VC.gameSceneView.presentScene(self.VC.gameScene)
  }
  private func gameScene(){
    self.VC.gameScene = SKScene()
    self.VC.gameScene.scaleMode = .aspectFill
  }
  private func wheelView(){
    let corner = self.VC.wheelView.bounds.width / 2
    self.VC.wheelView.cornerRadius(corner, true)
  }
}
//MARK: - Initial
extension SetupWheelBonus{
  
  //MARK: - Inition
  convenience init(viewController: WheelBonusViewController) {
    self.init()
    self.VC = viewController
  }
}

