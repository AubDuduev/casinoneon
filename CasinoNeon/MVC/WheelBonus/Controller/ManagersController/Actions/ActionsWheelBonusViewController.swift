
import UIKit

extension WheelBonusViewController {
  
  @IBAction func backButton(button: UIButton){
    self.dismiss(animated: true){
      self.callBack?()
    }
  }
  @IBAction func spinWheelButton(button: UIButton){
    self.controllers.logic.spinWheel()
  }
}
