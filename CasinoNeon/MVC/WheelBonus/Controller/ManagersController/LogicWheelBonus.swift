//
//  LogicWheelBonus.swift
import UIKit
import SpriteKit

class LogicWheelBonus: Controller {
  
  //MARK: - Public variable
  public var VC: WheelBonusViewController!
  
  public func spinWheel(){
    guard self.VC.controllers.present.trySpin() else {
      self.VC.dismiss(animated: true, completion: nil)
      return
    }
    self.VC.wheelView.spin()
    self.VC.spinButton.isEnabled = false
    DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
      self.VC.spinButton.isEnabled = true
    }
    //Set wheel number section
    let rondomIndex: Int = .random(in: 0...5)
    let gradus = CGFloat(self.VC.arrayBonus[rondomIndex])
    let summ   = self.VC.arraySumm[rondomIndex]
    self.VC.gameScene.run(SKAction.sound(file: .roulette))
    //Set rundom gradus wheel
    DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
      guard let self = self else { return }
      self.VC.wheelView.transform = CGAffineTransform(rotationAngle: .radians(gr: gradus))
      
      //Set music game
      DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
        guard let self = self else { return }
        self.VC.gameScene.run(SKAction.sound(file: .winRulete))
        self.setData(summ: summ)
        self.arrayElements(rondomIndex: rondomIndex)
      }
    }
  }
  private func setData(summ: Int){
    let currentSumm = StoreProject.shared.getInt(key: .currentScore) ?? 0
    let result = currentSumm + summ
    
    self.VC.balanceLabel.text = String(result)
    self.VC.winLabel.text     = String(summ)
    
    StoreProject.shared.saveInt(key: .currentScore, value: result)
  }
  public func spin(){
    self.VC.currentBonus += 5
    self.VC.wheelView.transform = CGAffineTransform(rotationAngle: .radians(gr:  CGFloat(self.VC.currentBonus)))
    self.VC.balanceLabel.text = String(self.VC.currentBonus)
  }
  public func arrayElements(rondomIndex: Int){
    self.VC.winsLabels.forEach({$0.textColor = .white})
    self.VC.winsImages.forEach({$0.transform = CGAffineTransform(scaleX: 1, y: 1)})
    
    self.VC.winsLabels[rondomIndex].textColor = .red
    self.VC.winsImages[rondomIndex].transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
  }
  
}
//MARK: - Initial
extension LogicWheelBonus{
  
  //MARK: - Inition
  convenience init(viewController: WheelBonusViewController) {
    self.init()
    self.VC = viewController
  }
}

