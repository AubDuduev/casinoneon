//
//  RouterWheelBonus.swift
import Foundation

class RouterWheelBonus: Controller {
  
  //MARK: - Public variable
  public var VC: WheelBonusViewController!
  
  
}
//MARK: - Initial
extension RouterWheelBonus{
  
  //MARK: - Inition
  convenience init(viewController: WheelBonusViewController) {
    self.init()
    self.VC = viewController
  }
}

