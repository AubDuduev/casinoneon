//
//  WheelBonusControllers.swift

import Foundation

class WheelBonusControllers: Controllers {
  
  let setup    : SetupWheelBonus!
  let server   : ServerWheelBonus!
  let present  : PresentWheelBonus!
  let logic    : LogicWheelBonus!
  let animation: AnimationWheelBonus!
  let router   : RouterWheelBonus!
  let vc       : WheelBonusViewController!
  
  init(viewController: WheelBonusViewController) {
    
    self.setup     = SetupWheelBonus(viewController: viewController)
    self.server    = ServerWheelBonus(viewController: viewController)
    self.present   = PresentWheelBonus(viewController: viewController)
    self.logic     = LogicWheelBonus(viewController: viewController)
    self.animation = AnimationWheelBonus(viewController: viewController)
    self.router    = RouterWheelBonus(viewController: viewController)
    self.vc        = viewController
  }
}
