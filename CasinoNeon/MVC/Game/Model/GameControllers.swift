
import Foundation

class GameControllers: Controllers {
  
  let setup    : SetupGame!
  let server   : ServerGame!
  let present  : PresentGame!
  let logic    : LogicGame!
  let animation: AnimationGame!
  let router   : RouterGame!
  let vc       : GameViewController!
  
  init(viewController: GameViewController) {
    
    self.setup     = SetupGame(viewController: viewController)
    self.server    = ServerGame(viewController: viewController)
    self.present   = PresentGame(viewController: viewController)
    self.logic     = LogicGame(viewController: viewController)
    self.animation = AnimationGame(viewController: viewController)
    self.router    = RouterGame(viewController: viewController)
    self.vc        = viewController
  }
}
