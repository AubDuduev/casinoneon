import UIKit
import SpriteKit

class GameViewController: UIViewController {
  
  //MARK: - Controllers
  public var controllers: GameControllers!
  
  //MARK: - Public variable
  public var gameSlotsScene     : GameSlotsScene!
  public var gameSlotsSceneView : SKView!
  public var payTableView       = PayTableView().loadNib()
  
  override var shouldAutorotate: Bool {
    return true
  }
  
  //MARK: - Outlets
  @IBOutlet weak var menuView       : UIView!
  @IBOutlet weak var boubleBetButton: UIButton!
  @IBOutlet weak var autoSpinButton : UIButton!
  @IBOutlet weak var spinButton     : UIButton!
  @IBOutlet weak var slotsView      : UIView!
  @IBOutlet weak var spinButtonView : UIView!
  @IBOutlet weak var buttonsView    : UIView!
  @IBOutlet weak var topView        : UIView!
  @IBOutlet weak var leftNumberView : UIView!
  @IBOutlet weak var rightNumberView: UIView!
  @IBOutlet weak var betLabel       : UILabel!
  @IBOutlet weak var lineLabel      : UILabel!
  @IBOutlet weak var balanceLabel   : UILabel!
  @IBOutlet weak var winLabel       : UILabel!
  @IBOutlet weak var titleImageView : UIImageView!
  
  //MARK: - Array outlets
  @IBOutlet var leftNumberLabels : [UILabel]!
  @IBOutlet var rightNumberLabels: [UILabel]!
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setControllers()
    self.controllers.setup.viewDidLoad()
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.controllers.setup.viewDidAppear()
  }
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    self.controllers.setup.viewDidLayoutSubviews()
  }
  private func setControllers(){
    self.controllers = GameControllers(viewController: self)
  }
}
