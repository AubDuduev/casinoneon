//
//  ActionsGameViewController.swift
//  Games
//
//  Created by Senior Developer on 16.12.2020.
//
import UIKit

extension GameViewController {
  
  @IBAction func menuButton(button: UIButton){
    self.controllers.logic.menuPush()
  }
  @IBAction func spinButton(button: UIButton){
    self.controllers.logic.spin()
  }
  @IBAction func betButton(button: UIButton){
    self.controllers.logic.bet(button: button)
  }
  @IBAction func lineButton(button: UIButton){
    self.controllers.logic.line(button: button)
  }
  @IBAction func payTableButton(button: UIButton){
    self.controllers.logic.payTable()
  }
  @IBAction func doubleBetButton(button: UIButton){
    self.controllers.logic.doubleBet()
  }
  @IBAction func autoSpinButton(button: UIButton){
    self.controllers.logic.autoSpin()
  }
}
