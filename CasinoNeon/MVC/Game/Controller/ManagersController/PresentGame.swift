import Foundation

class PresentGame: Controller {
  
  //MARK: - Public variable
  public var VC: GameViewController!
  
  
}
//MARK: - Initial
extension PresentGame {
  
  //MARK: - Inition
  convenience init(viewController: GameViewController) {
    self.init()
    self.VC = viewController
  }
}

