import UIKit
import SpriteKit
import SwiftyGif

class SetupGame: Controller {
  
  //MARK: - Public variable
  public var VC: GameViewController!
  
  public func viewDidLoad() {
    self.gameSlotsScene()
    self.gameSlotsSceneView()
    self.addedView()
    self.topImageGIFView()
  }
  public func viewDidAppear() {
    self.didViewScene()
  }
  public func viewDidLayoutSubviews() {
    //Настроика сцены слотов
    self.gameSlotsSceneViewConstraint()
    self.gameSlotsSceneConstraint()
    //Настройка кнокпи спин
    self.spinButtonView()
    self.slotsView()
    self.menuView()
  }
}
//MARK: - Private functions
extension SetupGame {
  
  private func addedView(){
    self.VC.slotsView.addSubview(self.VC.gameSlotsSceneView)
  }
  private func spinButtonView(){
    let corner = self.VC.spinButtonView.bounds.width / 2
    self.VC.spinButtonView.cornerRadius(corner, true)
    self.VC.spinButtonView.shadowColor(color: .black, radius: 10)
  }
  private func menuView(){
    self.VC.menuView.cornerRadius(10, true)
  }
}
//MARK: - Public functions GameSlotsScene
extension SetupGame {
  
  private func gameSlotsScene(){
    self.VC.gameSlotsScene = GameSlotsScene(fileNamed  : .GameSlots,
                                            controllers: self.VC.controllers)
    self.VC.gameSlotsScene.scaleMode       = .aspectFill
    self.VC.gameSlotsScene.backgroundColor = .black
  }
  private func gameSlotsSceneView(){
    self.VC.gameSlotsSceneView = SKView()
    self.VC.gameSlotsSceneView.backgroundColor  = .black
    self.VC.gameSlotsSceneView.shadowColor(color: #colorLiteral(red: 0.4788222016, green: 0.8576355909, blue: 1, alpha: 0.4), radius: 20)
    self.VC.gameSlotsSceneView.ignoresSiblingOrder = false
    self.VC.gameSlotsSceneView.showsFPS       = true
    self.VC.gameSlotsSceneView.showsNodeCount = true
  }
  private func didViewScene(){
    guard self.VC.gameSlotsScene.view == nil else { return }
    self.VC.gameSlotsSceneView.presentScene(self.VC.gameSlotsScene)
  }
  private func slotsView(){
    self.VC.slotsView.cornerRadius(7, false)
    self.VC.slotsView.shadowColor(color: #colorLiteral(red: 0.4788222016, green: 0.8576355909, blue: 1, alpha: 0.4), radius: 20)
  }
  private func gameSlotsSceneConstraint(){
    let height: CGFloat = self.VC.slotsView.frame.height
    let width : CGFloat = self.VC.slotsView.frame.width
    self.VC.gameSlotsScene?.size = CGSize(width: width, height: height)
  }
  private func gameSlotsSceneViewConstraint(){
    self.VC.gameSlotsSceneView.cornerRadius(7, true)
    self.VC.gameSlotsSceneView.translatesAutoresizingMaskIntoConstraints = false
    self.VC.gameSlotsSceneView.topAnchor.constraint(equalTo: self.VC.slotsView.topAnchor).isActive = true
    self.VC.gameSlotsSceneView.bottomAnchor.constraint(equalTo: self.VC.slotsView.bottomAnchor).isActive = true
    self.VC.gameSlotsSceneView.rightAnchor.constraint(equalTo: self.VC.slotsView.rightAnchor).isActive = true
    self.VC.gameSlotsSceneView.leadingAnchor.constraint(equalTo: self.VC.slotsView.leadingAnchor).isActive = true
  }
  private func topImageGIFView(){
    let gif = try! UIImage(gifName: "titleGames.gif")
    self.VC.titleImageView.setGifImage(gif, loopCount: -1)
    self.VC.titleImageView.startAnimatingGif()
  }
}
//MARK: - Initial
extension SetupGame {
  
  //MARK: - Inition
  convenience init(viewController: GameViewController) {
    self.init()
    self.VC = viewController
  }
}

