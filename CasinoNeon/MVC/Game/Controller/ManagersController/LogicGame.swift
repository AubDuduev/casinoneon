import UIKit
import SpriteKit

class LogicGame: Controller {
  
  //MARK: - Public variable
  public var VC: GameViewController!
  
  public func numberLine(color: UIColor, tag: Int){
    self.VC.leftNumberLabels.forEach ({$0.textColor = .white})
    self.VC.rightNumberLabels.forEach({$0.textColor = .white})
    
    self.VC.leftNumberLabels.filter ({$0.tag == tag}).first?.textColor = color
    self.VC.rightNumberLabels.filter({$0.tag == tag}).first?.textColor = color
  }
  public func spin(){
    self.VC.spinButton.isEnabled = false
    self.VC.gameSlotsScene.spin(){ [weak self] isSpin in
      guard let self = self else { return }
      self.VC.spinButton.isEnabled = true
    }
  }
  public func bet(button: UIButton){
    self.VC.betLabel.text = Score.shared.setBet(button: button)
  }
  public func line(button: UIButton){
    self.VC.lineLabel.text = Score.shared.setLine(button: button)
  }
  public func addScore(slotsNods: [SlotNode]){
    let win = Score.shared.winColculation(slotNodes: slotsNods)
    self.VC.winLabel.text     = String(win)
    self.VC.balanceLabel.text = Score.shared.setBalance(value: win)
  }
  public func zeroScore(){
    Score.shared.gameScene    = self.VC.gameSlotsScene
    self.VC.winLabel.text     = String(0)
    self.VC.balanceLabel.text = Score.shared.munisColculation()
  }
  public func menuPush(){
    self.VC.controllers.router.push(.MenuVC)
  }
  public func payTable(){
    self.VC.payTableView.frame = self.VC.view.frame
    self.VC.view.addSubview(self.VC.payTableView)
  }
  public func doubleBet(){
    if let winCurrent = Int(self.VC.winLabel.text ?? "0") {
      self.VC.balanceLabel.text = Score.shared.setBalance(value: -winCurrent)
      self.VC.controllers.router.push(.DoubleBet(winCurrent))
    }
  }
  public func wheelBonus(){
    DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
      guard let self = self else { return }
      self.VC.controllers.router.push(.WheelBonus)
    }
  }
  public func addScore(value: Int){
    self.VC.balanceLabel.text = Score.shared.setBalance(value: value)
  }
  public func autoSpin(){
    //1 - Проверяем есть ли баланс
    guard self.isBalance() else { return }
    //2 - Настраеваем кнопку спин
    self.VC.autoSpinButton.setTitleColor(.red, for: .normal)
    self.VC.autoSpinButton.isEnabled = false
    self.VC.gameSlotsScene.spin(){ [weak self] isSpin in
      guard let self = self else { return }
      //3 - Проверяем есть ли баланс
      guard self.isBalance() else {
        self.VC.autoSpinButton.setTitleColor(.white, for: .normal)
        self.VC.autoSpinButton.isEnabled = true
        return
      }
      //4 - Проверяем нужно ли вращать
      guard isSpin else {
        self.VC.autoSpinButton.setTitleColor(.white, for: .normal)
        self.VC.autoSpinButton.isEnabled = true
        return
      }
      //5 - Повторяем спин
      DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
        guard let self = self else { return }
        self.autoSpin()
      }
    }
  }
  public func isBalance() -> Bool {
    return (StoreProject.shared.getInt(key: .currentScore) ?? 0) > 0
  }
  public func isBonus(slotNodes: [SlotNode]?) -> Bool {
    if let slotNodes = slotNodes {
      return (slotNodes.filter({ Slots(rawValue: $0.imageNamed)! == .slotNodeBonus }).first != nil)
    } else {
      return false
    }
  }
  public func boubleBetButton(hidden: Bool){
    if hidden {
      self.VC.boubleBetButton.isEnabled = !hidden
      self.VC.boubleBetButton.setTitleColor(.white, for: .normal)
    } else {
      self.VC.boubleBetButton.isEnabled = !hidden
      self.VC.boubleBetButton.setTitleColor(.red, for: .normal) 
    }
  }
}
//MARK: - Initial
extension LogicGame{
  
  //MARK: - Inition
  convenience init(viewController: GameViewController) {
    self.init()
    self.VC = viewController
  }
}

