import Foundation

class RouterGame: Controller {
  
  //MARK: - Public variable
  public var VC: GameViewController!
  
  public func push(_ type: Push){
    
    switch type {
      case .DoubleBet(let betSumm):
        self.pushDoubleBet(betSumm: betSumm)
      case .WheelBonus:
        self.pushWheelBonusVC()
      case .MenuVC:
        self.pushMenuVC()
    }
  }
  
  enum Push {
    case DoubleBet(Int)
    case WheelBonus
    case MenuVC
  }
}
//MARK: - Private functions
extension RouterGame {
  
  private func pushDoubleBet(betSumm: Int){
    let doubleBetVC = self.VC.getVCForID(storyboardID     : .DoubleBet,
                                         vcID             : .DoubleBetVC,
                                         transitionStyle  : .crossDissolve,
                                         presentationStyle: .fullScreen) as! DoubleBetViewController
    doubleBetVC.betSumm  = betSumm
    doubleBetVC.bankSumm = betSumm
    doubleBetVC.callBack = { [weak self] win in
      guard let self = self else { return }
      self.VC.controllers.logic.addScore(value: win)
    }
    self.VC.present(doubleBetVC, animated: true)
  }
  private func pushWheelBonusVC(){
    let wheelBonusVC = self.VC.getVCForID(storyboardID     : .WheelBonus,
                                          vcID             : .WheelBonusVC,
                                          transitionStyle  : .crossDissolve,
                                          presentationStyle: .fullScreen) as! WheelBonusViewController
    wheelBonusVC.callBack = { [weak self] in
      guard let self = self else { return }
      self.VC.controllers.logic.addScore(value: 0)
    }
    self.VC.present(wheelBonusVC, animated: true)
  }
  private func pushMenuVC(){
    let menuVC = self.VC.getVCForID(storyboardID     : .Menu,
                                    vcID             : .MenuVC,
                                    transitionStyle  : .crossDissolve,
                                    presentationStyle: .fullScreen) as! MenuViewController
    menuVC.callBack = { [weak self] in
      guard let self = self else { return }
      self.VC.controllers.logic.addScore(value: 0)
    }
    self.VC.present(menuVC, animated: true)
  }
}
//MARK: - Initial
extension RouterGame{
  
  //MARK: - Inition
  convenience init(viewController: GameViewController) {
    self.init()
    self.VC = viewController
  }
}

