import Foundation

class AnimationGame: Controller {
  
  //MARK: - Public variable
  public var VC: GameViewController!
  
  
}
//MARK: - Initial
extension AnimationGame {
  
  //MARK: - Inition
  convenience init(viewController: GameViewController) {
    self.init()
    self.VC = viewController
  }
}

