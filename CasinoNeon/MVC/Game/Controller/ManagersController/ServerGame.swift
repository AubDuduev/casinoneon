import Foundation

class ServerGame: Controller {
  
  //MARK: - Public variable
  public var VC: GameViewController!
  
  
}
//MARK: - Initial
extension ServerGame{
  
  //MARK: - Inition
  convenience init(viewController: GameViewController) {
    self.init()
    self.VC = viewController
  }
}

