
import Foundation

class ServerLoading: Controller {
  
  //MARK: - Public variable
  public var VC: LoadingViewController!
  
  
}
//MARK: - Initial
extension ServerLoading {
  
  //MARK: - Inition
  convenience init(viewController: LoadingViewController) {
    self.init()
    self.VC = viewController
  }
}

