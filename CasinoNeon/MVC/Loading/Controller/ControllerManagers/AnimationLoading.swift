
import Foundation

class AnimationLoading: Controller {
  
  //MARK: - Public variable
  public var VC: LoadingViewController!
  
  
}
//MARK: - Initial
extension AnimationLoading {
  
  //MARK: - Inition
  convenience init(viewController: LoadingViewController) {
    self.init()
    self.VC = viewController
  }
}

