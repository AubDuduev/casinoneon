
import Foundation

class LogicLoading: Controller {
  
  //MARK: - Public variable
  public var VC: LoadingViewController!
  
  public func viewDidAppear() {
    self.openVC()
  }
  
}
//MARK: - Private functions
extension LogicLoading {
  
  private func openVC(){
    if !BonusDay.shared.isBonus() {
      self.VC.controllers.router.push(.Game)
    } else {
      self.VC.controllers.router.push(.CardBonus)
    }
  }
}
//MARK: - Initial
extension LogicLoading {
  
  //MARK: - Inition
  convenience init(viewController: LoadingViewController) {
    self.init()
    self.VC = viewController
  }
}

