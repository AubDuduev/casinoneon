
import UIKit

class RouterLoading: Controller {
  
  //MARK: - Public variable
  public var VC: LoadingViewController!
  
  public func push(_ type: Push){
    
    switch type {
      case .Game:
        self.pushGameVC()
      case .CardBonus:
        self.pushCardBonus()
    }
  }
  
  enum Push {
    case Game
    case CardBonus
  }
}
//MARK: - Private functions
extension RouterLoading{
  
  private func pushGameVC(){
    let gameVC = self.VC.getVCForID(storyboardID     : .Game,
                                    vcID             : .GameVC,
                                    transitionStyle  : .crossDissolve,
                                    presentationStyle: .fullScreen) as! GameViewController
    self.VC.present(gameVC, animated: true)
  }
  private func pushCardBonus(){
    let bonusDayVC = self.VC.getVCForID(storyboardID     : .CardBonus,
                                        vcID             : .CardBonusVC,
                                        transitionStyle  : .crossDissolve,
                                        presentationStyle: .fullScreen) as! CardBonusViewController
    self.VC.present(bonusDayVC, animated: true)
  }
}
//MARK: - Initial
extension RouterLoading {
  
  //MARK: - Inition
  convenience init(viewController: LoadingViewController) {
    self.init()
    self.VC = viewController
  }
}

