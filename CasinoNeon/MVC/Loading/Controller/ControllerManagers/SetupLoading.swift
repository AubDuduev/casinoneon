
import Foundation

class SetupLoading: Controller {
  
  //MARK: - Public variable
  public var VC: LoadingViewController!
  
  public func viewDidAppear() {
    self.setupMusic()
    self.scoreStart()
  }
}
//MARK: - Private functions
extension SetupLoading {
  
  private func setupMusic(){
    StoreProject.shared.saveBool(key: .isSound, value: true)
    StoreProject.shared.saveBool(key: .isMusic, value: false)
    GDAudioPlayer.shared.load(fileName: .backgraund, type: .mp3)
    GDAudioPlayer.shared.play()
  }
  private func scoreStart(){
    if StoreProject.shared.getInt(key: .currentScore) == nil {
      StoreProject.shared.saveInt(key: .currentScore, value: 3000)
    }
    if StoreProject.shared.getInt(key: .currentScore) == 0 {
      StoreProject.shared.saveInt(key: .currentScore, value: 3000)
    }
  }
}
//MARK: - Initial
extension SetupLoading {
  
  //MARK: - Inition
  convenience init(viewController: LoadingViewController) {
    self.init()
    self.VC = viewController
  }
}

