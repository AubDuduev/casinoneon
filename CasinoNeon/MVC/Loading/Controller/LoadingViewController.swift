
import UIKit

class LoadingViewController: UIViewController {
  
  //MARK: - Controllers
  public var controllers: LoadingControllers!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setControllers()
   
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.controllers.setup.viewDidAppear()
    self.controllers.logic.viewDidAppear()
  }
  private func setControllers(){
    self.controllers = LoadingControllers(viewController: self)
  }
}
