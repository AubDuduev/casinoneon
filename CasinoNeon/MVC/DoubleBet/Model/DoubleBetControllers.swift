import Foundation

class DoubleBetControllers: Controllers {
  
  let setup    : SetupDoubleBet!
  let server   : ServerDoubleBet!
  let present  : PresentDoubleBet!
  let logic    : LogicDoubleBet!
  let animation: AnimationDoubleBet!
  let router   : RouterDoubleBet!
  let vc       : DoubleBetViewController!
  
  init(viewController: DoubleBetViewController) {
    
    self.setup     = SetupDoubleBet(viewController: viewController)
    self.server    = ServerDoubleBet(viewController: viewController)
    self.present   = PresentDoubleBet(viewController: viewController)
    self.logic     = LogicDoubleBet(viewController: viewController)
    self.animation = AnimationDoubleBet(viewController: viewController)
    self.router    = RouterDoubleBet(viewController: viewController)
    self.vc        = viewController
  }
}
