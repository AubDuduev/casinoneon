
import UIKit

extension DoubleBetViewController {
  
  @IBAction func backButton(button: UIButton){
    self.controllers.logic.pickUp()
  }
  @IBAction func openPlayerCardButton(button: UIButton){
    self.controllers.logic.openPlayerCard(isOpen: true, indexCard: button.tag)
  }
  @IBAction func pickUpButton(button: UIButton){
    self.controllers.logic.pickUp()
  }
  @IBAction func doubleButton(button: UIButton){
    self.controllers.logic.double()
  }
}
