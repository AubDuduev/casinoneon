
import SpriteKit
import UIKit

class LogicDoubleBet: Controller {
  
  //MARK: - Public variable
  public var VC: DoubleBetViewController!
  
  public func viewDidLoad() {
    self.setLabelData()
    self.setCard()
  }
  
  public func viewDidAppear() {
    self.openEnemyCard(isOpen: true)
  }

  public func openEnemyCard(isOpen: Bool){
    let from = isOpen ? self.VC.enemyBackCardView!  : self.VC.enemyCardImageView!
    let to   = isOpen ? self.VC.enemyCardImageView! : self.VC.enemyBackCardView!
    self.VC.gameSlotsScene!.run(SKAction.sound(file: .backCard))
    UIView.transition(from: from, to: to, duration: 0.5, options: [.curveEaseIn, .transitionFlipFromLeft, .showHideTransitionViews])
  }
  public func openPlayerCard(isOpen: Bool, indexCard: Int){
    guard self.VC.cardGameState == .Game else { return }
    let from = isOpen ? self.VC.userBackCardsView[indexCard]   : self.VC.userCardsImagesView[indexCard]
    let to   = isOpen ? self.VC.userCardsImagesView[indexCard] : self.VC.userBackCardsView[indexCard]
    self.VC.gameSlotsScene!.run(SKAction.sound(file: .backCard))
    UIView.transition(from: from, to: to, duration: 0.5, options: [.curveEaseIn, .transitionFlipFromLeft, .showHideTransitionViews])
    guard isOpen else { return }
    self.colculationSumm(indexCard: indexCard)
    self.VC.currentIndexOpenCard = indexCard
  }
  public func setCard(){
    
    self.VC.enemyCard = Card.allCases.randomElement()!
    let oneCard       = Card.allCases.randomElement()!
    let twoCard       = Card.allCases.randomElement()!
    let threeCard     = Card.allCases.randomElement()!
    let foreCard      = Card.allCases.randomElement()!
    self.VC.playerCards.append(oneCard)
    self.VC.playerCards.append(twoCard)
    self.VC.playerCards.append(threeCard)
    self.VC.playerCards.append(foreCard)
    self.VC.enemyCardImageView.image     = self.VC.enemyCard.image()
    self.VC.userCardsImagesView[0].image = oneCard.image()
    self.VC.userCardsImagesView[1].image = twoCard.image()
    self.VC.userCardsImagesView[2].image = threeCard.image()
    self.VC.userCardsImagesView[3].image = foreCard.image()
  }
  public func colculationSumm(indexCard: Int){
    print("--1--")
    print(self.VC.bankSumm)
    print(self.VC.betSumm)
    print(self.VC.doubleSumm)
    //1 -
    self.VC.playerCard = self.VC.playerCards[indexCard]
    //2 -
    if self.VC.playerCard.rawValue > self.VC.enemyCard.rawValue {
      self.VC.bankSumm    = self.VC.bankSumm  * 2
      self.VC.doubleSumm  = self.VC.bankSumm  * 2
      self.VC.cardGameState = .Win
      self.VC.doubleButton.isEnabled = true
      self.setLabelData()
    //3 -
    } else if self.VC.playerCard.rawValue == self.VC.enemyCard.rawValue {
      DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
        guard let self = self else { return }
        self.openEnemyCard(isOpen: false)
        self.openPlayerCard(isOpen: false, indexCard: indexCard)
        self.VC.cardGameState = .Evil
        self.VC.doubleButton.isEnabled = true
        self.setLabelData()
      }
    //4 -
    } else {
      self.VC.bankSumm   = 0
      self.VC.betSumm    = 0
      self.VC.doubleSumm = 0
      self.VC.cardGameState = .Lose
      self.VC.doubleButton.isEnabled = false
      self.setLabelData()
    }
  }
  public func setLabelData(){
    self.VC.bankSummLabel.text   = String(self.VC.bankSumm)
    self.VC.betSummLabel.text    = String(self.VC.betSumm)
    self.VC.doubleSummLabel.text = String(self.VC.doubleSumm)
    print("--2--")
    print(self.VC.bankSumm)
    print(self.VC.betSumm)
    print(self.VC.doubleSumm)
  }
  public func double(){
    self.VC.cardGameState = .Game
    self.openEnemyCard(isOpen: false)
    self.openPlayerCard(isOpen: false, indexCard: self.VC.currentIndexOpenCard)
    self.setCard()
    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
      self.openEnemyCard(isOpen: true)
    }
    self.setCard()
  }
  public func pickUp(){
    self.VC.dismiss(animated: true) {
      self.VC.callBack(self.VC.bankSumm)
    }
  }
}
//MARK: - Initial
extension LogicDoubleBet{
  
  //MARK: - Inition
  convenience init(viewController: DoubleBetViewController) {
    self.init()
    self.VC = viewController
  }
}

