
import UIKit

class DoubleBetViewController: UIViewController {
  
  //MARK: - Controllers
  public var controllers: DoubleBetControllers!
  
  //MARK: - Public variable
  public var doubleSumm     = 0
  public var betSumm        = 0
  public var bankSumm       = 0
  public var enemyCard      : Card!
  public var playerCard     : Card!
  public var playerCards    = [Card]()
  public var playerIsOpen   = false
  public var cardGameState  : CardGameState = .Game
  public var gameSlotsScene = GameSlotsScene(fileNamed: .GameSlots)
  public var callBack       : Clousure<Int>!
  public var currentIndexOpenCard = 0
  
  //MARK: - Outlets
  @IBOutlet weak var doubleSummLabel   : UILabel!
  @IBOutlet weak var bankSummLabel     : UILabel!
  @IBOutlet weak var betSummLabel      : UILabel!
  @IBOutlet weak var enemyBackCardView : UIView!
  @IBOutlet weak var doubleButton      : UIButton!
  @IBOutlet weak var pickUpButton      : UIButton!
  @IBOutlet weak var enemyCardImageView: UIImageView!
  
  //MARK: - Outlets array
  @IBOutlet var userBackCardsView  : [UIView]!
  @IBOutlet var userCardsImagesView: [UIImageView]!
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setControllers()
    self.controllers.logic.viewDidLoad()
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.controllers.logic.viewDidAppear()
  }
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    self.controllers.setup.viewDidLayoutSubviews()
  }
  private func setControllers(){
    self.controllers = DoubleBetControllers(viewController: self)
  }
  
}
