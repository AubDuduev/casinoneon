//
//  CardsCollectionCell.swift
//  CasinoNeon
import UIKit

class CardsCollectionCell: UICollectionViewCell, LoadNidoble {
  
  private var controllers: CardBonusControllers!
  
  @IBOutlet weak var bonusLabel: UILabel!
  @IBOutlet weak var bonusView : UIView!
  @IBOutlet weak var backView  : UIView!
  
  public func configure(controllers: CardBonusControllers?){
    self.controllers = controllers
    let bonus = BonusDays.allCases.shuffled()
    self.bonusLabel.text = bonus.randomElement()?.rawValue
  }
  override func layoutSubviews() {
    super.layoutSubviews()
    self.cornerRadius(3, true)
    self.contentView.cornerRadius(3, true)
  }
  @IBAction func openCardButton(button: UIButton){
    self.controllers.logic.openEnemyCard(cell: self, isOpen: true)
  }
  
}
