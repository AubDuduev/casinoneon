//
//  CardBonusControllers.swift
//  CasinoNeon
//
//  Created by Senior Developer on 19.12.2020.
//

import Foundation

class CardBonusControllers: Controllers {
  
  let setup    : SetupCardBonus!
  let server   : ServerCardBonus!
  let present  : PresentCardBonus!
  let logic    : LogicCardBonus!
  let animation: AnimationCardBonus!
  let router   : RouterCardBonus!
  let vc       : CardBonusViewController!
  
  init(viewController: CardBonusViewController) {
    
    self.setup     = SetupCardBonus(viewController: viewController)
    self.server    = ServerCardBonus(viewController: viewController)
    self.present   = PresentCardBonus(viewController: viewController)
    self.logic     = LogicCardBonus(viewController: viewController)
    self.animation = AnimationCardBonus(viewController: viewController)
    self.router    = RouterCardBonus(viewController: viewController)
    self.vc        = viewController
  }
}
