
import SpriteKit
import UIKit

class CardBonusViewController: UIViewController {
  
  //MARK: - Controllers
  public var controllers: CardBonusControllers!
  
  //MARK: - Public variable
  public var gameScene    : SKScene!
  public var gameSceneView: SKView!
  public var cardsCollection = CardsCollection()
  
  //MARK: - Outlets
 
  @IBOutlet weak var cardsCollectionView: UICollectionView!
  @IBOutlet weak var collectionViewView : UIView!
  @IBOutlet weak var balanceLabel       : UILabel!
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setControllers()
    self.controllers.setup.viewDidLoad()
    self.controllers.present.viewDidLoad()
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
   
  }
  private func setControllers(){
    self.controllers = CardBonusControllers(viewController: self)
  }
}
