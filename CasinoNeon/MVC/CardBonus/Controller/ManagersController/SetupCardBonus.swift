import UIKit
import SpriteKit

class SetupCardBonus: Controller {
  
  //MARK: - Public variable
  public var VC: CardBonusViewController!
  
  public func viewDidLoad() {
    self.bonusDayCollection()
    self.collectionViewView()
    self.gameScene()
    self.gameSceneView()
  }
}
//MARK: - Private functions
extension SetupCardBonus {
  
  private func bonusDayCollection(){
    let collectionViewLayout = CardsCollectionViewLaytout()
    collectionViewLayout.sectionInset                 = .init(top: 0, left: 0, bottom: 0, right: 0)
    collectionViewLayout.minimumLineSpacing           = 0.5
    collectionViewLayout.minimumInteritemSpacing      = 0.5
    collectionViewLayout.sectionInsetReference        = .fromContentInset
    self.VC.cardsCollectionView.collectionViewLayout  = collectionViewLayout
    self.VC.cardsCollection.collectionView            = self.VC.cardsCollectionView
    self.VC.cardsCollection.controllers               = self.VC.controllers
    self.VC.cardsCollection.collectionView.delegate   = self.VC.cardsCollection
    self.VC.cardsCollection.collectionView.dataSource = self.VC.cardsCollection
  }
  private func collectionViewView(){
    self.VC.collectionViewView.cornerRadius(5, true)
    self.VC.collectionViewView.borderColor(.set(.titleColor), 1)
  }
  private func gameSceneView(){
    self.VC.gameSceneView                     = SKView()
    self.VC.gameSceneView.frame               = CGRect()
    self.VC.gameSceneView.ignoresSiblingOrder = false
    self.VC.gameSceneView.showsFPS            = true
    self.VC.gameSceneView.showsNodeCount      = true
    self.VC.gameSceneView.backgroundColor     = .clear
    self.VC.view.addSubview(self.VC.gameSceneView)
    self.VC.gameSceneView.presentScene(self.VC.gameScene)
  }
  private func gameScene(){
    self.VC.gameScene = SKScene()
    self.VC.gameScene.scaleMode = .aspectFill
  }
}
//MARK: - Initial
extension SetupCardBonus {
  
  //MARK: - Inition
  convenience init(viewController: CardBonusViewController) {
    self.init()
    self.VC = viewController
  }
}

