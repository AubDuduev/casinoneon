import SpriteKit
import Foundation

class LogicCardBonus: Controller {
  
  //MARK: - Public variable
  public var VC: CardBonusViewController!
  
  public func openEnemyCard(cell: CardsCollectionCell, isOpen: Bool){
    let from = isOpen ? cell.backView!  : cell.bonusView!
    let to   = isOpen ? cell.bonusView! : cell.backView!
    self.VC.gameScene.run(SKAction.sound(file: .backCard))
    UIView.transition(from: from, to: to, duration: 0.5, options: [.curveEaseIn, .transitionFlipFromLeft, .showHideTransitionViews])
    DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
      guard let self = self else { return }
      self.colculation(cell: cell)
    }
   
  }
  public func colculation(cell: CardsCollectionCell){
    var summ    = Int(cell.bonusLabel.text ?? "0") ?? 0
    var balance = Int(self.VC.balanceLabel.text ?? "0") ?? 0
    Timer.scheduledTimer(withTimeInterval: 0.002, repeats: true) { (timer) in
      summ -= 1
      balance += 1
      guard summ != -1  else {
        timer.invalidate()
        self.VC.controllers.router.push(.Game)
        return
      }
      self.VC.balanceLabel.text = String(balance)
      cell.bonusLabel.text      = String(summ)
      StoreProject.shared.saveInt(key: .currentScore, value: balance)
    }
  }
}
//MARK: - Initial
extension LogicCardBonus {
  
  //MARK: - Inition
  convenience init(viewController: CardBonusViewController) {
    self.init()
    self.VC = viewController
  }
}

