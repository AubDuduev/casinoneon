//
//  PresentCardBonus.swift
import Foundation

class PresentCardBonus: Controller {
  
  //MARK: - Public variable
  public var VC: CardBonusViewController!
  
  public func viewDidLoad() {
    self.setData()
  }
  
  private func setData(){
    let currentSumm = StoreProject.shared.getInt(key: .currentScore) ?? 0
    self.VC.balanceLabel.text = String(currentSumm)
  }
}
//MARK: - Initial
extension PresentCardBonus {
  
  //MARK: - Inition
  convenience init(viewController: CardBonusViewController) {
    self.init()
    self.VC = viewController
  }
}

