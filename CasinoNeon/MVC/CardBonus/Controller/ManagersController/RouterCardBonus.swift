
import UIKit
import Foundation

class RouterCardBonus: Controller {
  
  //MARK: - Public variable
  public var VC: CardBonusViewController!
  
  public func push(_ type: Push){
    
    switch type {
      case .Game:
        self.pushGameVC()
    }
  }
  
  enum Push {
    case Game
  }
}
//MARK: - Private functions
extension RouterCardBonus {
  
  private func pushGameVC(){
    let gameVC = self.VC.getVCForID(storyboardID     : .Game,
                                    vcID             : .GameVC,
                                    transitionStyle  : .crossDissolve,
                                    presentationStyle: .fullScreen) as! GameViewController
    UIApplication.shared.windows.first?.rootViewController = gameVC
    UIApplication.shared.windows.first?.makeKey()
    self.VC.present(gameVC, animated: true)
  }
}
//MARK: - Initial
extension RouterCardBonus {
  
  //MARK: - Inition
  convenience init(viewController: CardBonusViewController) {
    self.init()
    self.VC = viewController
  }
}


