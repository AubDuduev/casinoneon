//
//  AnimationCardBonus.swift
//  CasinoNeon

import Foundation

class AnimationCardBonus: Controller {
  
  //MARK: - Public variable
  public var VC: CardBonusViewController!
  
  
}
//MARK: - Initial
extension AnimationCardBonus{
  
  //MARK: - Inition
  convenience init(viewController: CardBonusViewController) {
    self.init()
    self.VC = viewController
  }
}

