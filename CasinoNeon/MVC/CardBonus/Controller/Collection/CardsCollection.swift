//
//  CardsCollection.swift
//  CasinoNeon
//
//  Created by Senior Developer on 19.12.2020.
//

import UIKit

class CardsCollection : NSObject {
  
  //MARK: - Variable
  public var collectionView: UICollectionView!
  public var controllers   : CardBonusControllers!
  public var bonuses       : [String]!
}

//MARK: - Delegate CollectionView
extension CardsCollection: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
  }
}

//MARK: - DataSource CollectionView
extension CardsCollection: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    self.collectionView = collectionView
    return self.bonuses?.count ?? 25
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = CardsCollectionCell().collectionCell(collectionView, indexPath: indexPath)
    cell.configure(controllers: controllers)
    return cell
  }
}
//MARK: - DelegateFlowLayout  CollectionView
extension CardsCollection: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let width : CGFloat = (collectionView.bounds.width  / 5) - (0.5 * 1.5)
    let height: CGFloat = (collectionView.bounds.height / 5) - (0.5 * 1)
    return .init(width: width, height: height)
  }
  
}


