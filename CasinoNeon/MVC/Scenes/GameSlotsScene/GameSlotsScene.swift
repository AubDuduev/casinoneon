import SpriteKit
import GameplayKit

class GameSlotsScene: SKScene {
   
   //Public
   public var controllers: GameControllers!
   
   //Private
   private var slotsColomnNode  : SlotsColomnNode!
   private var animationSlots   : AnimationSlots!
   private var createSlots      : CreateSlots!
   private var textureSlotsNode = AddTextureSlotsNode()
   private var createLineShape  : CreateLineShape!
   private var compareLines     = CompareLines()
   private var isSpin           = true
   
   
   override func didMove(to view: SKView) {
      super.didMove(to: view)
      self.createCreateSlots()
      self.createAnimationSlots()
      self.createCreateLineShape()
   }
   
   private func newFunc(){
      
   }
   private func createAnimationSlots(){
      self.animationSlots = AnimationSlots(gameSlotsScene: self)
   }
   private func createSlotsColomnNode(){
      self.slotsColomnNode = SlotsColomnNode(name: .SlotsColomnNode)
      self.slotsColomnNode.size = self.size
      self.slotsColomnNode.setup()
      self.addChild(self.slotsColomnNode)
   }
   public func createCreateSlots(){
      self.slotsColomnNode?.removeFromParent()
      self.createSlots?.slotNodes.forEach({$0.removeFromParent()})
      self.createSlots?.slotGameNodes.forEach({$0.removeFromParent()})
      self.createSlots = CreateSlots(gameSlotsScene: self)
      self.createSlots.create()
      self.createSlotsColomnNode()
   }
   private func createCreateLineShape(){
      self.createLineShape = CreateLineShape(gameSlotsScene: self)
   }
   public func spin(completion: @escaping Clousure<Bool>){
      //1 - Удаляем все линии если есть
      self.createLineShape.removeLine()
      self.controllers.logic.boubleBetButton(hidden: true)
      //2 - Обнуляем Score и отнимаем текущую ставку
      self.controllers.logic.zeroScore()
      //3 -
      self.run(SKAction.sound(file: .endShort))
      self.createCreateSlots()
      //4 - Делаем анимацию по вертикали слотов
      self.animationSlots.startAnimation(createSlots: self.createSlots)
      //5 - Добовляем анимацию из структур
      self.textureSlotsNode.add(slotNodes: self.createSlots.slotGameNodes)
      //6 - Ищем совпадение
      self.compareLines.compare(slotNodes: self.createSlots.slotGameNodes, delay: 7){ [weak self] nodes in
         guard let self = self else { return }
         //7 - Выделаем цветом совпавшие слоты
         CompareLines.Line.allCases.forEach { [weak self] in
            self?.compareLines.light(line: $0)
         }
         //8 - Рисуем линии совпадения
         self.run(SKAction.sound(file: .compare))
         self.createLineShape.create(nodes: nodes)
         //9 - Меняем цвет у цыфр
         self.createLineShape.setColorNumber()
         //10 -
         let slotNodes = self.compareLines.winSlotNodes()
         self.controllers.logic.addScore(slotsNods: slotNodes)
         self.controllers.logic.boubleBetButton(hidden: false)
         //11 -
         completion(!self.controllers.logic.isBonus(slotNodes: slotNodes))
      }
   }
   convenience init?(fileNamed: SceneName, controllers: GameControllers) {
      self.init(fileNamed: fileNamed.rawValue)
      self.controllers = controllers
   }
}
