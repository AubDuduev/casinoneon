
import Foundation

class MenuControllers: Controllers {
  
  let setup    : SetupMenu!
  let server   : ServerMenu!
  let present  : PresentMenu!
  let logic    : LogicMenu!
  let animation: AnimationMenu!
  let router   : RouterMenu!
  let vc       : MenuViewController!
  
  init(viewController: MenuViewController) {
    
    self.setup     = SetupMenu(viewController: viewController)
    self.server    = ServerMenu(viewController: viewController)
    self.present   = PresentMenu(viewController: viewController)
    self.logic     = LogicMenu(viewController: viewController)
    self.animation = AnimationMenu(viewController: viewController)
    self.router    = RouterMenu(viewController: viewController)
    self.vc        = viewController
  }
}
