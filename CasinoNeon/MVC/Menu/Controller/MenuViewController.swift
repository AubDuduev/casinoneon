
import UIKit

class MenuViewController: UIViewController {
  
  //MARK: - Controllers
  public var controllers: MenuControllers!
  
  //MARK: - Public variable
  public var payTableView = PayTableView().loadNib()
  public var callBack     : ClousureEmpty!
  
  //MARK: - Outlets
  @IBOutlet weak var balanceLabel    : UILabel!
  @IBOutlet weak var soundOnOffButton: UIButton!
  @IBOutlet weak var musicOnOffButton: UIButton!
  
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setControllers()
    self.controllers.setup.viewDidLoad()
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.controllers.setup.viewDidAppear()
  }
  private func setControllers(){
    self.controllers = MenuControllers(viewController: self)
  }
}
