
import Foundation

class RouterMenu: Controller {
  
  //MARK: - Public variable
  public var VC: MenuViewController!
  
  
}
//MARK: - Initial
extension RouterMenu{
  
  //MARK: - Inition
  convenience init(viewController: MenuViewController) {
    self.init()
    self.VC = viewController
  }
}

