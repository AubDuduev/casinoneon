
import UIKit
import Foundation

class LogicMenu: Controller {
  
  //MARK: - Public variable
  public var VC: MenuViewController!
  
  public func muteValumeButton(button: UIButton){
    if button.currentBackgroundImage == #imageLiteral(resourceName: "buttonsOnSound") {
      button.setBackgroundImage(#imageLiteral(resourceName: "buttonsOffSound"), for: .normal)
    } else {
      button.setBackgroundImage(#imageLiteral(resourceName: "buttonsOnSound"), for: .normal)
    }
  }
  public func swipeGameSound(button: UIButton){
    if button.currentBackgroundImage == #imageLiteral(resourceName: "switchOnBlack") {
      StoreProject.shared.saveBool(key: .isSound, value: false)
      button.setBackgroundImage(#imageLiteral(resourceName: "switchOffBlack"), for: .normal)
    } else {
      StoreProject.shared.saveBool(key: .isSound, value: true)
      button.setBackgroundImage(#imageLiteral(resourceName: "switchOnBlack"), for: .normal)
    }
  }
  public func swipeGameMusic(button: UIButton){
    if button.currentBackgroundImage == #imageLiteral(resourceName: "switchOnBlack") {
      StoreProject.shared.saveBool(key: .isMusic, value: false)
      button.setBackgroundImage(#imageLiteral(resourceName: "switchOffBlack"), for: .normal)
      GDAudioPlayer.shared.pause()
    } else {
      StoreProject.shared.saveBool(key: .isMusic, value: true)
      button.setBackgroundImage(#imageLiteral(resourceName: "switchOnBlack"), for: .normal)
      GDAudioPlayer.shared.load(fileName: .backgraund, type: .mp3)
      GDAudioPlayer.shared.play()
    }
  }
  public func payTable(){
    self.VC.payTableView.frame = self.VC.view.frame
    self.VC.view.addSubview(self.VC.payTableView)
  }
}
//MARK: - Initial
extension LogicMenu{
  
  //MARK: - Inition
  convenience init(viewController: MenuViewController) {
    self.init()
    self.VC = viewController
  }
}

