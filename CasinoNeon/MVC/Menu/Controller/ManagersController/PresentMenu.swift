
import Foundation

class PresentMenu: Controller {
  
  //MARK: - Public variable
  public var VC: MenuViewController!
  
  
}
//MARK: - Initial
extension PresentMenu{
  
  //MARK: - Inition
  convenience init(viewController: MenuViewController) {
    self.init()
    self.VC = viewController
  }
}

