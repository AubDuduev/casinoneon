
import UIKit
import Foundation

class SetupMenu: Controller {
  
  //MARK: - Public variable
  public var VC: MenuViewController!
  
  public func viewDidLoad() {
    self.swipeGameSound()
    self.swipeGameMusic()
  }
  public func viewDidAppear() {
    self.balanceLabel()
  }
	public func newFunc(){
	
	}
}
extension SetupMenu {
  
  public func swipeGameSound(){
    if !StoreProject.shared.getBool(key: .isSound) {
      StoreProject.shared.saveBool(key: .isSound, value: false)
      self.VC.soundOnOffButton.setBackgroundImage(#imageLiteral(resourceName: "switchOff"), for: .normal)
    } else {
      StoreProject.shared.saveBool(key: .isSound, value: true)
      self.VC.soundOnOffButton.setBackgroundImage(#imageLiteral(resourceName: "switchOn"), for: .normal)
    }
  }
  public func swipeGameMusic(){
    if !StoreProject.shared.getBool(key: .isMusic) {
      StoreProject.shared.saveBool(key: .isMusic, value: false)
      self.VC.musicOnOffButton.setBackgroundImage(#imageLiteral(resourceName: "switchOff"), for: .normal)
      GDAudioPlayer.shared.pause()
    } else {
      StoreProject.shared.saveBool(key: .isMusic, value: true)
      self.VC.musicOnOffButton.setBackgroundImage(#imageLiteral(resourceName: "switchOn"), for: .normal)
      GDAudioPlayer.shared.load(fileName: .backgraund, type: .mp3)
      GDAudioPlayer.shared.play()
    }
  }
  private func balanceLabel(){
    self.VC.balanceLabel.text = Score.shared.setBalance(value: 0)
  }
}
//MARK: - Initial
extension SetupMenu{
  
  //MARK: - Inition
  convenience init(viewController: MenuViewController) {
    self.init()
    self.VC = viewController
  }
}

