
import UIKit

extension MenuViewController {
  
  @IBAction func backButton(button: UIButton){
    self.dismiss(animated: true){
      self.callBack?()
    }
  }
  @IBAction func soundSwipeGesure(button: UIButton){
    self.controllers.logic.swipeGameSound(button: button)
  }
  @IBAction func musicSwipeButton(button: UIButton){
    self.controllers.logic.swipeGameMusic(button: button)
  }
  @IBAction func payTableButton(button: UIButton){
    self.controllers.logic.payTable()
  }
}
