
import UIKit
import AVFoundation

class GDSound {
  
  static let shared      = GDSound()
  
  private var timerSound = Timer()
  
  func play(idSound: SoundID){
    AudioServicesPlaySystemSound(SystemSoundID(idSound.rawValue))
  }
  //функция воспроизвидения сигналов
  func playCustomSound(idSound: SoundID, repeatC: Int, sec: Double){
    
    var repeatTimer = 0
    timerSound = Timer.scheduledTimer(withTimeInterval: sec, repeats: true) {(selfTimer) in
      AudioServicesPlaySystemSound(SystemSoundID(idSound.rawValue))
      repeatTimer += 1
      guard repeatC == repeatTimer else { return }
      selfTimer.invalidate()
    }
  }
  //стоп звук
  func stopSound(){
    timerSound.invalidate()
  }
  enum SoundID: Double {
    case keyBoardTap   = 1104.0
    case vibration     = 4095.0
    case startTreaning = 1003.0
    case stopTreaning  = 1013.0
    case alertTreaning = 1054.0
    case tenSecond_1   = 1255.0
    case tenSecond_2   = 1259.0
    case recreation    = 1309.0
    case box           = 1313.0
    case relaxTime     = 1009.0
  }
}
