
import UIKit

class WheelView: UIView {
  
  //MARK: - Private variable
  private var gradus  = 0.0
  private var centerX = Double(0)
  private var centerY = Double(0)
  private var radius  = Double(0)
  private var labelX  = Double(0)
  private var labelY  = Double(50)
  private var index   = Int(0)
  private var transfo = 90.0
  
  //MARK: - Setup WheelView
  public func setup(count: Int){
    self.centerX = Double(self.frame.width  / 2) - 40
    self.centerY = Double(self.frame.height / 2) - 25
    self.radius  = Double(self.frame.width  / 2) - 50
    self.labelX  = Double(self.frame.width  / 2) - 15
    self.labelY  = (Double(self.frame.height / 2) - self.radius)
  }
  //MARK: - Spin Animation
  public func spin() {
    let rotation = CABasicAnimation(keyPath: "transform.rotation.z")
    rotation.fromValue = NSNumber(value: Double.pi * 1)
    rotation.toValue   = NSNumber(value: Double.pi * 30)
    rotation.duration  = 3
    rotation.isCumulative = true
    rotation.repeatCount = 1
    self.layer.add(rotation, forKey: "rotationAnimation")
  }
}

