
import SceneKit
import SpriteKit
import Foundation

class SlotNode: SKSpriteNode {

  private var gameFon       : SKSpriteNode!
  private let bitMask       = BitMasks()
  private var gameSceneSlots: GameSlotsScene!
  
  public var colomn    : Int!
  public var row       : Int!
  public var index     : Int!
  public var imageNamed: String!
  
  public var positionX: CGFloat!
  public var positionY: CGFloat!
  
  public func setup(colomn: Int, row: Int, index: Int, imageNamed: String?, scene: GameSlotsScene) {
    self.imageNamed     = imageNamed
    self.colomn         = colomn
    self.row            = row
    self.index          = index
    self.gameSceneSlots = scene
    self.name           = "slotNode"
  }
  
  public func light(color: UIColor){
    self.color   = .white
    let colorOne = SKAction.colorize(with: color , colorBlendFactor: 1.0, duration: 0.2)
    let colorTwo = SKAction.colorize(with: .white, colorBlendFactor: 1.0, duration: 0.2)
    let group    = SKAction.group([colorOne, colorTwo, colorOne, colorTwo])
    let repeatAC = SKAction.repeat(group, count: 5)
    self.run(repeatAC){
      self.run(colorTwo)
    }
  }
  override init(texture: SKTexture?, color: UIColor, size: CGSize) {
    super.init(texture: texture, color: color, size: size)
  }
  convenience init(imageNamed: String, position: CGPoint, slotSquareSize: CGSize) {
    let texture = SKTexture(imageNamed: imageNamed)
    self.init(texture: texture, color: .clear, size: .zero)
    self.position    = position
    self.size        = slotSquareSize
    self.physicsBody = SKPhysicsBody(rectangleOf: self.size)
    self.isUserInteractionEnabled = true
    self.physicsBody?.isDynamic   = true
    self.physicsBody?.contactTestBitMask = self.bitMask.value(.gameFon)
    self.physicsBody?.collisionBitMask   = self.bitMask.value(.gameFon)
    self.physicsBody?.categoryBitMask    = self.bitMask.value(.diamond)
    self.physicsBody?.affectedByGravity  = false
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
