
import UIKit
import SpriteKit

class Score {
  
  static let shared = Score()
  
  private var win     = 0
  private var balance = StoreProject.shared.getInt(key: .currentScore) ?? 3000
  private var bet     = 10
  private var line    = 1
  private var bonus   = 0
  
  public var gameScene: GameSlotsScene!
  
  public func setBet(button: UIButton?) -> String {
    switch button?.tag {
      case 0:
        guard bet != 10 else { return String(bet) }
        bet -= 10
      case 1:
        guard bet != 100 else { return String(bet) }
        bet += 10
      default:
        break
    }
  return String(bet)
  }
  public func setLine(button: UIButton) -> String {
    switch button.tag {
      case 0:
        guard line != 1 else { return String(line) }
        line -= 1
      case 1:
        guard line != 9 else { return String(line)}
        line += 1
      default:
        break
    }
    return String(line)
  }
  public func setBalance(value: Int) -> String {
    self.balance = StoreProject.shared.getInt(key: .currentScore) ?? 3000
    self.balance += value
    StoreProject.shared.saveInt(key: .currentScore, value: self.balance)
   return String(balance)
  }
  public func winColculation(slotNodes: [SlotNode]) -> Int {
    //Cчитаем какие иконки выпали и умножаем количество линий на число которое выиграли
    self.colculaion(slotNodes: slotNodes)
    //Делаем подсчет
    self.win = self.line * (self.bet * self.bonus)
    self.balance += self.win
    StoreProject.shared.saveInt(key: .currentScore, value: self.balance)
    return self.win
  }
  public func maxBet() -> (bet: String, line: String){
    self.bet  = 100
    self.line = 9
    return (String(self.win), String(self.line))
  }
  public func munisColculation() -> String {
    let result = self.bet * self.line
    self.balance -= result
    self.bonus = 0
    self.win   = 0
    StoreProject.shared.saveInt(key: .currentScore, value: self.balance)
    return String(self.balance)
  }
  
  private func colculaion(slotNodes: [SlotNode]){
    slotNodes.forEach { self.chengeBonus(string: $0.imageNamed )}
  }
  private func chengeBonus(string: String){
    
      switch Slots(rawValue: string)! {
        case .slotNode5 :
          self.bonus += 2
        case .slotNode6:
          self.bonus += 4
        case .slotNode7:
          self.bonus += 6
        case .slotNode8:
          self.bonus += 8
        case .slotNode9:
          self.bonus += 10
        case .slotNodeGold:
          self.bonus += 50
        case .slotNodeBonus:
          self.bonus += 50
          self.gameScene.run(SKAction.sound(file: .bonus))
          self.gameScene.controllers.logic.wheelBonus()
        default:
          self.bonus = 1
      }
  }
}
