
import SpriteKit

class WinLine {
  
  var winConbinationoble: WinConbinationoble!
 
  public func set(line: Win, slotNode: SlotNode, slotImage: Slots) -> [String]{
    switch line {
      case .one:
        self.winConbinationoble = WinOneLine()
      case .two:
        self.winConbinationoble = WinTwoLine()
      case .three:
        self.winConbinationoble = WinThreeLine()
      case .fore:
        self.winConbinationoble = WinForeLine()
      case .five:
        self.winConbinationoble = WinFiveLine()
      case .six:
        self.winConbinationoble = WinSixLine()
      case .seven:
        self.winConbinationoble = WinSevenLine()
      case .eight:
        self.winConbinationoble = WinEightLine()
      case .nine:
        self.winConbinationoble = WinNineLine()
    }
    return self.winConbinationoble.set(slotNode: slotNode, slotImage: slotImage)
  }

  
  enum Win: CaseIterable {
    case one
    case two
    case three
    case fore
    case five
    case six
    case seven
    case eight
    case nine
  }
  
}
