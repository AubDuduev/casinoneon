
import SpriteKit

class WinOneLine: WinConbinationoble {
  
  public func set(slotNode: SlotNode, slotImage: Slots) -> [String] {
    
    var slotsTexture = Slots.slots().shuffled()
    
    if slotNode.row == 1 && slotNode.colomn == 0 {
      slotsTexture.append(slotImage)
    }
    if slotNode.row == 1 && slotNode.colomn == 1 {
      slotsTexture.append(slotImage)
    }
    if slotNode.row == 1 && slotNode.colomn == 2 {
      slotsTexture.append(slotImage)
    }
    if slotNode.row == 1 && slotNode.colomn == 3 {
      slotsTexture.append(slotImage)
    }
    if slotNode.row == 1 && slotNode.colomn == 4 {
      slotsTexture.append(slotImage)
    }
    let stringTexture = slotsTexture.map({ $0.rawValue})
    return stringTexture
  }
}
