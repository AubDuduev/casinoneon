
import Foundation

class BitMasks {
  
  
  enum Types: UInt32 {
    
    case diamond    = 1
    case gameFon    = 2
    case backgraund = 4
  }
  
  public func value(_ maskType: Types) -> UInt32 {
    return maskType.rawValue
  }
}
