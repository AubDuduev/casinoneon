
import SpriteKit
import Foundation

class CreateLineShape {
   
   private let gameSlotsScene: GameSlotsScene!
   private var lineNumber    : Int!
   private var colorNumber   : UIColor!
   private var arrayLine     = [SKShapeNode]()
   
   public func removeLine(){
      self.arrayLine.forEach({$0.removeFromParent()})
   }
   
   public func create(nodes: [[CompareLines.Line: [SlotNode]]]){
      
      for slots in nodes {
         let slotNodes = slots.map({ $0.value}).flatMap({$0})
         var points    = slotNodes.compactMap({ $0.position })
         let line      = SKShapeNode(points: &points, count: points.count)
         let slotLine  = slots.keys.compactMap({ $0}).first!
         self.gameSlotsScene.addChild(line)
         switch slotLine {
            case .one:
               self.lineNumber  = 1
               self.colorNumber = .red
               line.strokeColor = self.colorNumber
            case .two:
               self.lineNumber  = 2
               self.colorNumber = .blue
               line.strokeColor = self.colorNumber
            case .three:
               self.lineNumber  = 3
               self.colorNumber = .brown
               line.strokeColor = self.colorNumber
            case .fore:
               self.lineNumber  = 4
               self.colorNumber = .green
               line.strokeColor = self.colorNumber
            case .five:
               self.lineNumber  = 5
               self.colorNumber = .magenta
               line.strokeColor = self.colorNumber
            case .six:
               self.lineNumber  = 6
               self.colorNumber = .purple
               line.strokeColor = self.colorNumber
            case .seven:
               self.lineNumber  = 7
               self.colorNumber = .orange
               line.strokeColor = self.colorNumber
            case .eight:
               self.lineNumber  = 8
               self.colorNumber = .yellow
               line.strokeColor = self.colorNumber
            case .nine:
               self.lineNumber  = 9
               self.colorNumber = #colorLiteral(red: 0.4513868093, green: 0.9930960536, blue: 1, alpha: 1)
               line.strokeColor = self.colorNumber
         }
         
         line.lineWidth = 3
         line.lineCap   = .round
         self.arrayLine.append(line)
      }
   }
   
   public func setColorNumber(){
      guard self.colorNumber != nil, self.lineNumber != nil else { return }
      self.gameSlotsScene.controllers.logic.numberLine(color: self.colorNumber, tag: self.lineNumber)
   }
   
   init(gameSlotsScene: GameSlotsScene) {
      self.gameSlotsScene = gameSlotsScene
      self.colorNumber    = .red
   }
}
