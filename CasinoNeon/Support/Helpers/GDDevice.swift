
import DeviceKit
import Foundation

class GDDevice {
  
  static let shared = GDDevice()
  
  public func type() ->  Device {
    let device = Device.current
    print(device)
    switch device {
      //1 - iPhone 5
      case .iPhone5, .iPhone5c, .iPhone5s, .iPhoneSE, .iPhoneSE2, .simulator(.iPhone5), .simulator(.iPhone5c), .simulator(.iPhone5s), .simulator(.iPhoneSE), .simulator(.iPhoneSE2):
        return .iPhone5
      //2 - iPad 7
      case .simulator(.iPad7), .simulator(.iPadPro9Inch), .simulator(.iPadAir3), .simulator(.iPadPro12Inch4):
        return .iPhoneSE2
      //3 - iPad Air
      case  .iPadAir, .iPadAir2, .iPadAir3:
        return .iPadAir
      //4 - iPadPro 9Inch
      case  .iPadPro9Inch:
        return .iPadPro9Inch
      //5 - iPadPro 11Inch
      case .iPadPro11Inch, .iPadPro11Inch2:
        return .iPadPro11Inch
      //6 - iPadPro 12Inch
      case  .iPadPro12Inch, .iPadPro12Inch2, .iPadPro12Inch3, .iPadPro12Inch4:
        return .iPadPro12Inch
      //7 - iPad Mini
      case  .iPadMini, .iPadMini2, .iPadMini3, .iPadMini4:
        return .iPadMini
      //8 - iPad 2
      case  .iPad2, .iPad3, .iPad4, .iPad5, .iPad6, .iPad7:
        return .iPad2
      //9 - iPhone 7
      case .iPhone6, .iPhone6s, .iPhone7, .iPhone8, .simulator(.iPhone6), .simulator(.iPhone6s), .simulator(.iPhone7), .simulator(.iPhone8):
        return .iPhone7
      //10 - iPhone X
      case .iPhoneX, .iPhoneXS, .iPhoneXR, .iPhone11, .iPhone11Pro, .iPhone12, .iPhone12Pro, .simulator(.iPhoneX), .simulator(.iPhoneXS), .simulator(.iPhoneXR), .simulator(.iPhone11), .simulator(.iPhone11Pro), .simulator(.iPhone12), .simulator(.iPhone12Pro):
        return .iPhoneX
      //11 - iPhone 11 MAX
      case .iPhone11ProMax, .iPhoneXSMax, .simulator(.iPhone11ProMax), .simulator(.iPhoneXSMax):
        return .iPhone11ProMax
      case .iPhone12ProMax, .simulator(.iPhone12ProMax):
        return .iPhone12ProMax
      //12 - iPhone Plus
      case .iPhone6Plus, .iPhone6sPlus, .iPhone8Plus, .iPhone7Plus, .simulator(.iPhone6Plus), .simulator(.iPhone6sPlus), .simulator(.iPhone8Plus), .simulator(.iPhone7Plus):
        return .iPhone6Plus
      //9 - Error
      default:
        return .unknown("error")
    }
  }
}
