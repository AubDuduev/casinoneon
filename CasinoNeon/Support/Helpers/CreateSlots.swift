
import SceneKit
import Foundation

class CreateSlots {
  
  private var slotNode      : SlotNode!
  private var gameSlotsScene: GameSlotsScene!
  
  public var slotGameNodes = [SlotNode]()
  public var slotNodes     = [SlotNode]()
  public var slotWidth     : CGFloat!
  public var slotHeight    : CGFloat!
  public let slotsInColomn : CGFloat = 10
  
  //Отступ
  let paddingY: CGFloat = 10
  let paddingX: CGFloat = 10
  
  //Линии между слотами
  var lineY: CGFloat = 0
  var lineX: CGFloat = 0
  
  public func create(){
    //Чиcло которое является количеством ячеек
    let countColomn  : CGFloat = 5
    let countLine    : CGFloat = 3
    let countSlots   = Int(slotsInColomn * countColomn)
   
    //Размер слота
    slotHeight = (self.gameSlotsScene.size.height / countLine)   - (paddingY * 2)
    slotWidth  = (self.gameSlotsScene.size.width  / countColomn) - (paddingX * 2)
   
    //Линии между слотами
    lineY = self.gameSlotsScene.size.height - ((paddingY * 2) + (slotHeight * countLine))
    lineX = self.gameSlotsScene.size.width  - ((paddingX * 2) + (slotWidth  * countColomn))
    lineY = lineY / 2
    lineX = lineX / 4
    //Картинки слотов
    let imageNames = Slots.allCases.shuffled()
    //Размер брилианта
    let slotSquareSize = CGSize(width: slotWidth, height: slotHeight)
    //Расположение первого слота
    var transformPositionY = -((self.gameSlotsScene.size.height / 2) - (slotHeight / 2))
    var transformPositionX = -((self.gameSlotsScene.size.width / 2) - (slotWidth  / 2))
    transformPositionY += paddingY
    transformPositionX += paddingX
    var transformPosition  = CGPoint(x: transformPositionX, y: transformPositionY)
    
    var colomn = 0
    var row    = Int((-slotsInColomn) + CGFloat(5))
    //Создаем и распалогаем слоты
    for index in 0..<countSlots {
      colomn = (index % Int(countColomn))
     
      let imageNamed  = imageNames.randomElement()!.rawValue
      let slotNode    = SlotNode(imageNamed    : imageNamed,
                                 position      : transformPosition,
                                 slotSquareSize: slotSquareSize)
      slotNode.positionY = transformPosition.y
      slotNode.positionX = transformPosition.x
      slotNode.setup(colomn    : colomn,
                     row       : row,
                     index     : index,
                     imageNamed: imageNamed,
                     scene     : self.gameSlotsScene)
      
      //Создаем массивы с слотами
      if row == 0 || row == 1 || row == 2 {
        self.slotGameNodes.append(slotNode)
      }
      self.slotNodes.append(slotNode)
     
      if colomn == (Int(countColomn) - 1) {
        transformPosition.x = transformPositionX
        transformPosition.y += (slotHeight + lineY)
        row += 1
      } else {
        transformPosition.x += (slotWidth + lineX)
      }
      self.gameSlotsScene.addChild(slotNode)
    }
  }
  init(gameSlotsScene: GameSlotsScene) {
    self.gameSlotsScene = gameSlotsScene
  }
}
