import Foundation
import SpriteKit

class AddTextureSlotsNode {
  
  private var winPeriod: Int!
  private let winLine  = WinLine()
  
  public func add(slotNodes: [SlotNode]){
   
    //Параметры линий
    let randomWinLine = WinLine.Win.allCases.randomElement()!
    let randomImage   = Slots.allCases.randomElement()!
    self.winPeriod    = .random(in: 0...3)
    
    //1 - Проходимся по каждому слоту
    for (index, slotNode) in slotNodes.enumerated() {
      //2 - Создаем массив с названиями картинок для слотов
      let stringTexture = self.randomGameLogic(index    : index,
                                               line     : randomWinLine,
                                               slotNode : slotNode,
                                               slotImage: randomImage) // назначаем линию выиграша
      //3 - Переводим этот масиив в структуры
      let textures        = SKTexture.textures(stringTexture)
      //4 - Назначаем имя слоту из последнего елемента масива анимации
      slotNode.imageNamed = stringTexture.last
      slotNode.texture    = SKTexture(imageNamed: stringTexture.last!)
      //5 - Делаем анимацию слота из массива текстур
      slotNode.animationTexture(textures: textures, time: 0.1, type: .One)
    }
  }
  private func randomGameLogic(index: Int, line: WinLine.Win, slotNode: SlotNode, slotImage: Slots) -> [String]{
    if winPeriod == 0 {
      //назначаем линию выигрыша и картинку для выигрыша
      return self.winLine.set(line: line, slotNode: slotNode, slotImage: slotImage)
    } else {
      //назначаем случайную линию
      return Slots.shuffled()
    }
  }
}
