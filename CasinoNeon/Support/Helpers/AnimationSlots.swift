
import UIKit
import SpriteKit

class AnimationSlots {
  
  private let gameSlotsScene   : GameSlotsScene
  private var slotsNodesCount  = 0
  
  public func startAnimation(createSlots: CreateSlots){
    let slotsNodesСolomn1 = createSlots.slotNodes.filter({ $0.colomn == 0 })
    let slotsNodesСolomn2 = createSlots.slotNodes.filter({ $0.colomn == 1 })
    let slotsNodesСolomn3 = createSlots.slotNodes.filter({ $0.colomn == 2 })
    let slotsNodesСolomn4 = createSlots.slotNodes.filter({ $0.colomn == 3 })
    let slotsNodesСolomn5 = createSlots.slotNodes.filter({ $0.colomn == 4 })
    
    self.slotsNodesCount = Int(createSlots.slotsInColomn) - 4
    
    //1 колона
    DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
      slotsNodesСolomn1.forEach { self?.animation(slotNode: $0) }
      self?.gameSlotsScene.run(SKAction.sound(file: .spin))
    }
    //2 колона
    DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
      slotsNodesСolomn2.forEach({self?.animation(slotNode: $0)})
      self?.gameSlotsScene.run(SKAction.sound(file: .spin))
    }
    //3 колона
    DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [weak self] in
      slotsNodesСolomn3.forEach({self?.animation(slotNode: $0)})
      self?.gameSlotsScene.run(SKAction.sound(file: .spin))
    }
    //4 колона
    DispatchQueue.main.asyncAfter(deadline: .now() + 4) { [weak self] in
      slotsNodesСolomn4.forEach({self?.animation(slotNode: $0)})
      self?.gameSlotsScene.run(SKAction.sound(file: .spin))
    }
    //5 колона
    DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
      slotsNodesСolomn5.forEach({self?.animation(slotNode: $0)})
      self?.gameSlotsScene.run(SKAction.sound(file: .spin))
    }
  }
  
  private func animation(slotNode: SlotNode){
    //Настроики анимации
    let square   = CGFloat((gameSlotsScene.size.height / 3) + 1.2)
    let distance = CGFloat(Int(square) * slotsNodesCount)
    let time     = CGFloat(0.1)
    let duration = TimeInterval((distance / square) * time)
    let moveY    = -CGFloat(distance)
    //анимация вниз
    let downMove = SKAction.moveBy(x: 0, y: moveY, duration: duration)
    downMove.timingMode = .easeInEaseOut
    downMove.timingFunction = { time in
      return simd_smoothstep(0, 1, time)
    }
    slotNode.run(downMove){
      //анимация вверх
      let upMove = SKAction.moveBy(x: 0, y: square, duration: 0.7)
      upMove.timingMode = .easeIn
      slotNode.run(upMove)
    }
  }
  init(gameSlotsScene: GameSlotsScene) {
    self.gameSlotsScene = gameSlotsScene
  }
}
