//
//  BonusDay.swift
import Foundation

class BonusDay {
  
  static let shared = BonusDay()
  
  private let calendar = Calendar.current
  
  public func isBonus() -> Bool {
    
    //1 - Пытаемся получить сегодняшний день
    let currentDateStaring = Date().getDateStaring(format: .monthDayYear)
   
    if StoreProject.shared.getString(key: .isBonusDay) == currentDateStaring {
    //2 - Получили значит бонус уже использован
      return false
    } else {
      //3 - Не получили значит бонус еще сегодня не использован
      StoreProject.shared.saveString(key: .isBonusDay, value: currentDateStaring)
      return true
    }
  }

}
