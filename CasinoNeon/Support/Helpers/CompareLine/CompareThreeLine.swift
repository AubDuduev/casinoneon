
import Foundation

class CompareThreeLine {
  
  public var slotsCompare = [SlotNode]()
  
  private let slots: [SlotNode]!
  
  public func compare(){
    let one   = self.slots.filter({ $0.row == 2}).filter({ $0.colomn == 0}).first!
    let two   = self.slots.filter({ $0.row == 2}).filter({ $0.colomn == 1}).first!
    let three = self.slots.filter({ $0.row == 2}).filter({ $0.colomn == 2}).first!
    let fore  = self.slots.filter({ $0.row == 2}).filter({ $0.colomn == 3}).first!
    let five  = self.slots.filter({ $0.row == 2}).filter({ $0.colomn == 4}).first!
    
    if one.imageNamed == two.imageNamed || two.imageNamed == Slots.slotNodeGold.rawValue {
      if one.imageNamed == three.imageNamed || three.imageNamed == Slots.slotNodeGold.rawValue {
        if one.imageNamed == fore.imageNamed || fore.imageNamed == Slots.slotNodeGold.rawValue {
          if one.imageNamed == five.imageNamed || five.imageNamed == Slots.slotNodeGold.rawValue {
            slotsCompare.append(one)
            slotsCompare.append(two)
            slotsCompare.append(three)
            slotsCompare.append(fore)
            slotsCompare.append(five)
          }
        }
      }
    }
  }
  
  public func light(){
    self.slotsCompare.forEach({$0.light(color: .set(.lineColor))})
  }
  init(slots: [SlotNode]) {
    self.slots = slots
  }
}
