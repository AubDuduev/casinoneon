
import Foundation

class CompareLines {
  
  private var allCompareSlots = [[Line: [SlotNode]]]()
  private var slotNodes       = [SlotNode]()
  private var compareOneLine  : CompareOneLine!
  private var compareTwoLine  : CompareTwoLine!
  private var compareThreeLine: CompareThreeLine!
  private var compareForeLine : CompareForeLine!
  private var compareFiveLine : CompareFiveLine!
  private var compareSixLine  : CompareSixLine!
  private var compareSevenLine: CompareSevenLine!
  private var compareEightLine: CompareEightLine!
  private var compareNineLine : CompareNineLine!
  
  public func compare(slotNodes: [SlotNode], delay: TimeInterval, completion: @escaping Clousure<[[Line: [SlotNode]]]>){
    DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
      for line in Line.allCases {
        self.compare(line: line, slotNodes: slotNodes)
      }
      completion(self.shape())
    }
  }
  
  private func compare(line: Line, slotNodes: [SlotNode]){
    switch line {
      
      case .one:
        self.compareOneLine = CompareOneLine(slots: slotNodes)
        self.compareOneLine.compare()
        if !self.compareOneLine.slotsCompare.isEmpty {
          let elements: [Line: [SlotNode]] = [.one: self.compareOneLine.slotsCompare]
          allCompareSlots.append(elements)
          //Добовляем слот выиграшный
          if let node = self.compareOneLine.slotsCompare.first {
            self.slotNodes.append(node)
          }
        }
      case .two:
        self.compareTwoLine = CompareTwoLine(slots: slotNodes)
        self.compareTwoLine.compare()
        if !self.compareTwoLine.slotsCompare.isEmpty {
          let elements: [Line: [SlotNode]] = [.two: self.compareTwoLine.slotsCompare]
          allCompareSlots.append(elements)
          //Добовляем слот выиграшный
          if let node = self.compareTwoLine.slotsCompare.first {
            self.slotNodes.append(node)
          }
        }
      case .three:
        self.compareThreeLine = CompareThreeLine(slots: slotNodes)
        self.compareThreeLine.compare()
        if !self.compareThreeLine.slotsCompare.isEmpty {
          let elements: [Line: [SlotNode]] = [.three: self.compareThreeLine.slotsCompare]
          allCompareSlots.append(elements)
          //Добовляем слот выиграшный
          if let node = self.compareThreeLine.slotsCompare.first {
            self.slotNodes.append(node)
          }
        }
      case .fore:
        self.compareForeLine = CompareForeLine(slots: slotNodes)
        self.compareForeLine.compare()
        if !self.compareForeLine.slotsCompare.isEmpty {
          let elements: [Line: [SlotNode]] = [.fore: self.compareForeLine.slotsCompare]
          allCompareSlots.append(elements)
          //Добовляем слот выиграшный
          if let node = self.compareForeLine.slotsCompare.first {
            self.slotNodes.append(node)
          }
        }
      case .five:
        self.compareFiveLine = CompareFiveLine(slots: slotNodes)
        self.compareFiveLine.compare()
        if !self.compareFiveLine.slotsCompare.isEmpty {
          let elements: [Line: [SlotNode]] = [.five: self.compareFiveLine.slotsCompare]
          allCompareSlots.append(elements)
          //Добовляем слот выиграшный
          if let node = self.compareFiveLine.slotsCompare.first {
            self.slotNodes.append(node)
          }
        }
      case .six:
        self.compareSixLine = CompareSixLine(slots: slotNodes)
        self.compareSixLine.compare()
        if !self.compareSixLine.slotsCompare.isEmpty {
          let elements: [Line: [SlotNode]] = [.six: self.compareSixLine.slotsCompare]
          allCompareSlots.append(elements)
          //Добовляем слот выиграшный
          if let node = self.compareSixLine.slotsCompare.first {
            self.slotNodes.append(node)
          }
        }
      case .seven:
        self.compareSevenLine = CompareSevenLine(slots: slotNodes)
        self.compareSevenLine.compare()
        if !self.compareSevenLine.slotsCompare.isEmpty {
          let elements: [Line: [SlotNode]] = [.seven: self.compareSevenLine.slotsCompare]
          allCompareSlots.append(elements)
          //Добовляем слот выиграшный
          if let node = self.compareSevenLine.slotsCompare.first {
            self.slotNodes.append(node)
          }
        }
      case .eight:
        self.compareEightLine = CompareEightLine(slots: slotNodes)
        self.compareEightLine.compare()
        if !self.compareEightLine.slotsCompare.isEmpty {
          let elements: [Line: [SlotNode]] = [.eight: self.compareEightLine.slotsCompare]
          allCompareSlots.append(elements)
          //Добовляем слот выиграшный
          if let node = self.compareEightLine.slotsCompare.first {
            self.slotNodes.append(node)
          }
        }
      case .nine:
        self.compareNineLine = CompareNineLine(slots: slotNodes)
        self.compareNineLine.compare()
        if !self.compareNineLine.slotsCompare.isEmpty {
          let elements: [Line: [SlotNode]] = [.nine: self.compareNineLine.slotsCompare]
          allCompareSlots.append(elements)
          //Добовляем слот выиграшный
          if let node = self.compareNineLine.slotsCompare.first {
            self.slotNodes.append(node)
          }
        }
    }
  }
  public func light(line: Line){
  
    switch line {
      case .one:
        self.compareOneLine.light()
      case .two:
        self.compareTwoLine.light()
      case .three:
        self.compareThreeLine.light()
      case .fore:
        self.compareForeLine.light()
      case .five:
        self.compareFiveLine.light()
      case .six:
        self.compareSixLine.light()
      case .seven:
        self.compareSevenLine.light()
      case .eight:
        self.compareEightLine.light()
      case .nine:
        self.compareNineLine.light()
    }
  }
  private func shape() -> [[Line: [SlotNode]]]{
    let array = self.allCompareSlots
    self.allCompareSlots.removeAll()
    return array
  }
  public func winSlotNodes() -> [SlotNode] {
    let array = self.slotNodes
    self.slotNodes.removeAll()
    return array
  }
  enum Line: CaseIterable {
    case one
    case two
    case three
    case fore
    case five
    case six
    case seven
    case eight
    case nine
  }
}
