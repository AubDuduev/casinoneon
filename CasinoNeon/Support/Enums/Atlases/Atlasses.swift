import SpriteKit
import Foundation

enum Atlases: String {
  
  case HeroHit
  case HeroHitPDF
  case GraundTrava
  
  static func textures(_ atlas: Self) -> [SKTexture] {
    
    let textureAtlas = SKTextureAtlas(named: atlas.rawValue)
    let sortedTextures = textureAtlas.textureNames.sorted()
    let textures = sortedTextures.compactMap {SKTexture(imageNamed: $0)}
    return textures
  }
}
