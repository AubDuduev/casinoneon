import Foundation

enum Slots: String, CaseIterable {
  
  case slotNode1
  case slotNode2
  case slotNode3
  case slotNode4
  case slotNode5
  case slotNode6
  case slotNode7
  case slotNode8
  case slotNode9
  case slotNodeBonus
  case slotNodeGold
  
  static func string() -> [String]{
    return Self.allCases.map{ $0.rawValue }
  }
  static func shuffled() -> [String]{
    let array = Self.allCases.map{ $0.rawValue }
    return array.shuffled()
  }
  static func slots() -> [Self]{
    return Self.allCases.map{ $0}
  }
}
