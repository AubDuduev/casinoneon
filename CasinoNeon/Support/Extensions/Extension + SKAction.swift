
import SpriteKit

extension SKAction {
  
  static func sound(file: SoundFile) -> SKAction {
    if StoreProject.shared.getBool(key: .isSound) {
      return SKAction.playSoundFileNamed(file.rawValue, waitForCompletion: false)
    } else {
      return SKAction.wait(forDuration: 0)
    }
  }
}
