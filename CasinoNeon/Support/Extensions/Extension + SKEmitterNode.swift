
import SpriteKit

extension SKEmitterNode {
  
  convenience init(fileNamed: EmitterName) {
    self.init(fileNamed: fileNamed.rawValue)!
  }
}
