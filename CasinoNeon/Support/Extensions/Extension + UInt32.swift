
import Foundation

extension UInt32 {
  
  
  enum BitMask: UInt32 {
    
    case Eagle    = 1
    case Bird     = 2
    case FireBall = 3
  }
  
  func bitMask(_ maskType: BitMask) -> UInt32 {
    return maskType.rawValue
  }
}
